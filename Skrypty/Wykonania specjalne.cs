IsCanBeEmpty = true;
string[] allowedOptions = new string[] {"0,05%", "HS", "-40...80°C", "PZH", "Tlen", "Hastelloy", "IP65", "3.1", "2.1" };//"0,025%"
string result = "";
foreach (string option in allowedOptions)
{
	string optionText = GetOneDeviceFeature(option, false);
	if(!string.IsNullOrWhiteSpace(optionText))
	{
		if(!string.IsNullOrWhiteSpace(result))
		{
				result +="/";
		}
		switch (optionText)
		{
			case "-40...80°C": result += "-40"; break;
			case "IP65": result += "PD"; break;
			default: result += optionText; break;
		}
	}
}
Set(result);