IsCanBeEmpty = true;

string text = GetOneDeviceFeature("Cyfrowa termokompensacja", false);
if(string.IsNullOrWhiteSpace(text))
{
    text = GetOneDeviceFeature("Rozszerzona termokompensacja", false) ?? "";
}
Append(ref text, GetOneDeviceFeature("Typ kabla", false), "/ ");
Append(ref text, GetOneDeviceFeature("Dodatkowy czujnik", false), "/ ");
Append(ref text, GetOneDeviceFeature("Region produkcji", false), "/ ");
Set(text);