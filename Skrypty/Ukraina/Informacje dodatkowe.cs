IsCanBeEmpty = true;
string text = GetOneDeviceFeature("Cyfrowa termokompensacja", false);
if(string.IsNullOrWhiteSpace(text))
{
    text = GetOneDeviceFeature("Rozszerzona termokompensacja", false) ?? "";
}
Append(ref text, GetOneDeviceFeature("Wykonanie tlenowe", false), "/ ");
Append(ref text, GetOneDeviceFeature("Klasa urządzenia", false), "/ ");
Set(text);