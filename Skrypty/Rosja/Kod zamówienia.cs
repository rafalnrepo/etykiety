IsCanBeEmpty = (GetResource("Tytuł - kod zamówienia")?? "").Length<2;
if(!IsCanBeEmpty)
{
    IsEnabledBackupMarketingCode = true;
    Set(MarketingSalesCode);
}