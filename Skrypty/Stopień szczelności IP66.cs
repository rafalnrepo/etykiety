﻿string text = "";
Append(ref text, GetOneDeviceFeature("Stopień szczelności", false));
if(string.IsNullOrWhiteSpace(text))
{
    Set("IP66");
}
else
{
    Set(text);
}